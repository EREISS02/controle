#include <stdio.h>
#include <stdlib.h>

int main()
{
    //question 1
    int i=1;
    while(i<=10)
    {
        printf("%d\n", i);
        i++;
    }
    for (i=1; i<=10; i++)
    {
        printf("%d\n", i);
    }

    //question 2

    //Le define ne stocke pas la valeur dans sa m�moire, et associe une valeur � un mot
    //La constante occupe une place dans la m�moire

    //question 3
    printf("\n\n");
    int choix;
    printf("1/Pizza quatre saison\n2/Pizza napolitaine\n3/Pizza orientale\n");
    printf("Choississez votre pizza\n");
    scanf("%d", &choix);
    switch(choix)
    {
    case 1 :
        printf("Pizza quatre saisons\n");
        break;
    case 2 :
        printf("Pizza napolitaine\n");
        break;
    case 3 :
        printf("Pizza orientale\n");
        break;
    default :
        printf("Cette pizza n'est pas sur le menu.\n");
        break;
    }
    if (choix == 1)
    {
        printf("Pizza quatre saisons\n");
    }
    else if (choix == 2)
    {
        printf("Pizza napolitaine\n");
    }
    else if (choix == 3)
    {
        printf("Pizza orientale\n");
    }
    else
    {
        printf("Cette pizza n'est pas sur le menu.\n");
    }


    return 0;
}
