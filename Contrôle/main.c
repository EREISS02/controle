#include <stdio.h>
#include <stdlib.h>
#include <time.h>

unsigned int jetons = 10; //nombre de jetons avec lesquels le joueur commence
const int jetonsnecessaires = 100; //nombre de jetons que le joueur devra atteindre pour pouvoir sortir du casino
unsigned int roulette; //nombre qui sera g�n�r� al�atoirement
unsigned int miseroulette; //case sur laquelle le joueur mise
unsigned int misevalide =1; //test pour savoir si la mise est comprise en 1 et 37
unsigned int misejetons; //nombre de jetons que le joueur souhaitera miser
unsigned int rouletterussejouer; //si le joueur veut continuer la roulette russe
int rouletterusse[6]= {0,0,0,0,0,0}; //nombre al�atoire compris entre 1 et 6
unsigned int vivant=1; //le joueur est vivant
int i;

int victoire()
{
    printf("Vous gagnez le double de votre mise.\n");
    jetons = jetons + misejetons * 2; //le joueur gagne le double de sa mise
    misevalide = 0; //sort de la boucle pour relancer une partie (tant qu'il reste des jetons au joueur)
    return jetons;
}

int froulette()
{
    while (jetons > 0 && jetons < jetonsnecessaires) //tant que le joueur a des jetons, et moins de 100
    {

        misevalide = 1;
        while(misevalide == 1 && jetons < jetonsnecessaires)//tant que la mise est valide et que le joueur n'a pas atteint 100
        {
            printf("Sur quelle couleur souhaitez-vous miser ?(1 pour rouge, 2 pour noir)\n");
            scanf("%d", &miseroulette); //demande la case o� le joueur souhaite miser ses jetons
            printf("Vous possedez %d jetons\n", jetons);
            printf("Combien de jetons voulez-vous miser ?\n");
            scanf("%d", &misejetons); //demande combien de jetons le joueur souhaite miser

            if (miseroulette<1 || miseroulette>2 || misejetons>jetons || misejetons>25 || misejetons<1) //test si la mise est valide ou non
            {
                printf("Votre mise n'est pas valide.\n");
                misevalide=0;//revient au d�but de la boucle
            }
            else //si la mise est valide on test la victoire
            {
                if (miseroulette == 1) //test si la couleur que le joueur a choisi, afin de lui renvoyer ce qu'il a choisi (sous forme de texte)
                {
                    printf("Vous avez mise %d jetons sur une case rouge.\n", misejetons);
                }
                else
                {
                    printf("Vous avez mise %d jetons sur une case noir.\n", misejetons);
                }
                jetons = jetons - misejetons; //on enl�ve la mise du joueur de ses jetons totaux
                roulette = rand()%38; //g�n�ration du nombre al�atoire de la roulette
                if (roulette==0) //teste si la roulette est tomb� sur la case verte (0)
                {
                    printf("Vert\n");
                }
                else
                {
                    if (roulette%2==0) //teste si la roulette est tomb� sur une case rouge
                    {
                        printf("La roulette tombe sur une case rouge.\n");
                    }
                    else //si la roulette est tomb� sur une case noire
                    {
                        printf("La roulette tombe sur une case noire.\n");
                    }
                }

                if (roulette ==0) //si le roulette tombe sur 0, le joueur perd sa mise
                {
                    printf("Vous perdez votre mise.\n");
                    misevalide = 0; //sort de la boucle pour relancer une partie (tant qu'il reste des jetons au joueur)
                }
                else
                {
                    if (roulette%2 == 0 && miseroulette%2 != 0) //si la case de la mise et de la roulette sont tous deux pairs = le joueur gagne
                    {
                        victoire();
                    }
                    else
                    {
                        if (roulette%2 != 0 && miseroulette%2 == 0) //si la case de la mise et de la roulette sont tous deux impairs = le joueur gagne
                        {
                            victoire();
                        }
                        else //si l'un est impair et l'autre pair = le joueur perd
                        {
                            printf("Vous perdez votre mise.\n");
                            misevalide = 0; //sort de la boucle pour relancer une partie (tant qu'il reste des jetons au joueur)
                        }

                    }
                }
            }
        }
    }
    return jetons;
}

int frouletterusse()
{
    i=0;
    while(jetons ==0)//tant que le joueur n'a pas de jetons, il doit jouer � la roulette russe
    {
        printf("Etant arrive a 0 jeton, la mafia vous force a jouer a la roulette russe pour sauver votre peau.\nVous gagnerez 20 jetons si vous survivez.\nUne goutte de sueur coule sur votre front...\n");
        rouletterusse[rand()%6]=1; //met la balle dans une chambre al�atoire du barillet
        rouletterussejouer = 1;
        while(rouletterussejouer == 1 && jetons<jetonsnecessaires)//tant que le joueur veut joueur
        {
            if(rouletterusse[i]==1) //si la chambre du barillet est pleine => le coup part
            {
                printf("Bienvenue au paradis.\n");
                vivant = 0; //le joueur est mort
                return 0;
            }
            else //si la chambre du barillet est vide =>le joueur survit
            {
                printf("Vous entendez un *clic* et survivez...\n");
                i++; //le barillet tourne, la balle se rapproche
                jetons = jetons + 20; //le joueur gagne 20 jetons
                if(jetons<jetonsnecessaires)
                {
                    printf("Vous avez %d jetons\nVoulez-vous retenter votre chancer a la roulette russe ?\n(Tapez 1 pour oui, ou un autre nombre pour retourner a la roulette)\n", jetons);
                    scanf("%d", &rouletterussejouer); //on demande au joueur s'il souhaite rejour, s'il ne souhaite pas rejouer, il retournera jouer � la roulette
                }
            }
        }
    }
    return jetons;
}


int main()
{
    srand(time(NULL));

    printf("Vous vous reveillez dans un casino tenu par la mafia russe.\nApres une serie de malchance, vous etes endette de 100 jetons.\nVous echangez votre montre contre 10 jetons.\nSi vous souhaitez vous en sortir en vie, il va falloir jouer a la roulette... ou la roulette russe.\n");

    while (jetons < jetonsnecessaires && vivant == 1) //tant que le joueur n'a pas 100 jetons et est vivant
    {
        froulette();
        frouletterusse();
    }
    if (vivant ==1 && jetons >= jetonsnecessaires) //test si le joueur est vivant et qu'il a au moins 100 jetons
    {
        printf("La mafia vous casse le bras avant de vous relacher, vous suspectant d'avoir triche.\nVous etes libre !");
    }
    return 0;
}


